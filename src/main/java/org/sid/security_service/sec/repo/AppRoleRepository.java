package org.sid.security_service.sec.repo;
import org.sid.security_service.sec.entities.AppRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AppRoleRepository extends JpaRepository<AppRole,Long> {
    AppRole findByRollName(String rollName);

}
