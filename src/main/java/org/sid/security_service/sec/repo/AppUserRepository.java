package org.sid.security_service.sec.repo;
import org.sid.security_service.sec.entities.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AppUserRepository  extends JpaRepository<AppUser,Long> {
    AppUser findByUsername(String username);
}
