package org.sid.security_service.sec.service;
import org.sid.security_service.sec.entities.AppRole;
import org.sid.security_service.sec.entities.AppUser;
import org.springframework.stereotype.Service;
import java.util.List;

public interface AccountService {
    AppUser addNewUser(AppUser appUser);
    AppRole addNewRole(AppRole appRole);
    void addRoleToUser(String username , String rollName);
    AppUser loadUserByUsername(String username);
    List<AppUser> listUsers();
}
